<?php
include 'database.php';
class M_TinTuc extends database 
{
    //get slide
    public function getSlide(){
        $sql="select * from slide";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    //get menu
    public function getMenu(){
        $sql="SELECT tl.*, GROUP_CONCAT(DISTINCT lt.id,':',lt.TenKhongDau,':',lt.Ten) 
        AS LoaiTin,tt.id AS idTin,tt.TieuDe AS TieuDeTin,tt.Hinh AS HinhTin,tt.TomTat AS TomTatTin,tt.TieuDeKhongDau AS TieuDeKhongDauTin
        FROM theloai tl 
        INNER JOIN loaitin lt 
        ON lt.idTheLoai=tl.id
        INNER JOIN tintuc tt 
        ON tt.idLoaiTin=lt.id
        GROUP BY tl.id";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    //get loai tin by IdLoai tin
    public function getLoaiTinByIdLoai($idLoai){
        $sql="select * from tintuc where idLoaiTin=$idLoai";
        $this->setQuery($sql);
        return $this->loadAllRows(array($idLoai));
    }
    //get title by id
    public function getTitleById($id){
        $sql="select Ten from loaitin where id=$id";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    //get content
    public function getContent($id){
        $sql="select * from tintuc where id=$id";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    //get comment
    public function getComment($idTin){
        $sql="select * from comment where idTinTuc=$idTin";
        $this->setQuery($sql);
        return $this->loadAllRows(array($idTin));
    }

    public function getNoiBat(){
        $sql="select * from tintuc where NoiBat=1 limit 0,5";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }

    public function getLienQuan($tenKhongDau){
        $sql="select tt.*  
            from theloai tl inner join tintuc tt on tl.id=tt.idLoaiTin
            where TenKhongDau=? limit 0,5";
        $this->setQuery($sql);
        return $this->loadAllRows(array($tenKhongDau));
    }
}

?>