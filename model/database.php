<?php 
class database{
    public $_sql;
    public $_dbh;
    public $_cursor;
    
    public function database(){
        try{
        $this->_dbh=new PDO('mysql:host=localhost; dbname=tin_tuc','tin_tuc_user','01668232327');
        }catch(Exception $e){
            var_dump($e);
        }
        
        $this->_dbh->query('set names "utf8"');
    }
    //set the query
    public function setQuery($sql){
        $this->_sql=$sql;
    }
    //execute the query
    public function execute($options=array()){
        $this->_cursor=$this->_dbh->prepare($this->_sql);
        if($options){
            for ($i=0; $i <count($options) ; $i++) { 
                $this->_cursor->bindParam($i+1,$options[$i]);
            }
        }
        $this->_cursor->execute();
        return $this->_cursor;
    }
    //load datas on the table
    public function loadAllRows($option=array()){
        if(!$option){
            if(!$result=$this->execute()){
                return false;
            }
        }else{
            if(!$result=$this->execute($option)){
                return false;
            }
        }
        return $result->fetchAll(PDO::FETCH_OBJ);
    }
    //load 1 data on the table
    public function loadRow($option=array()){
        if(!$option){
            if(!$result=$this->execute()){
                return false;
            }
        }else{
            if(!$result=$this->execute($option)){
                return false;
            }
        }
        return $result->fetch(PDO::FETCH_OBJ);
    }
    //load record
    public function loadRecord($option=array()){
        if(!$option){
            if(!$result=$this->execute()){
                return false;
            }
        }else{
            if(!$result=$this->execute($option)){
                return false;
            }
        }
        return $result->fetch(PDO::FETCH_OBJ);
    }
    //get lastid
    public function getLastId(){
        return $this->_dbh->lastInsertId();
    }
    //close connect to database
    public function disconnect(){
        $this->_dbh=null;
    }
}
?>