<?php 
include 'model/M_TinTuc.php';
    class C_TinTuc
    {
        //index page
        public function index(){
            $M_TinTuc=new M_TinTuc();
            $slide=$M_TinTuc->getSlide();
            $menu=$M_TinTuc->getMenu();
            return array('slide'=>$slide,'menu'=>$menu);
        }
        //loai tin
        public function DanhMucTin(){
            $idLoai=$_GET['idLoai'];
            $M_TinTuc=new M_TinTuc();
            $list=$M_TinTuc->getLoaiTinByIdLoai(($idLoai));
            $menu=$M_TinTuc->getMenu();
            $TieuDe=$M_TinTuc->getTitleById($idLoai);
            return array('danhmuctin'=>$list,'menu'=>$menu,'tieude'=>$TieuDe);
        }
        //trang chi tiet
        public function chiTiet(){
            $id=$_GET['idTin'];
            $tenKhongDau=$_GET['tenKhongDau'];
            $M_TinTuc=new M_TinTuc();
            $content=$M_TinTuc->getContent($id);
            $comment=$M_TinTuc->getComment($id);
            $noiBat=$M_TinTuc->getNoiBat();
            $lienQuan=$M_TinTuc->getLienQuan($tenKhongDau);
            return array('chitiettin'=>$content,'binhluan'=>$comment,'noiBat'=>$noiBat,'lienQuan'=>$lienQuan);
        }

    }
?>